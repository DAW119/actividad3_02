/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package a4_3;
import java.util.Scanner;
/**
 *
 * @author DAW121
 */
public class Curso {
    private String grupo;
    private String[] nombre;
    
     public Curso(String grupo,int numAlumnos) {
        Scanner teclado=new Scanner(System.in);
        this.grupo=grupo;
        this.nombre=new String[numAlumnos];
        for(int i=0;i<nombre.length;i++){
            System.out.println("Nombre del alumno"+(i+1)+": ");
            nombre[i]=teclado.next();
        }
    }
     
     public String iniciales(){
         String aux="";
         for(int i=0;i<nombre.length;i++){
             if (nombre[i].length()!=0)
                 aux+=nombre[i].charAt(0)+".\n";
         }
         return aux;
     }
     
     public void desplaza(){
         String aux=nombre[nombre.length-1];
         for(int i=nombre.length-2;i>=0;i--){
             nombre[i+1]=nombre[i];
             
     }
         nombre[0]=aux;
     }
     
     public String verNombre(int n){
         String aux="";
            if(n<=nombre.length) aux=nombre[n-1];
         return aux;
     } 
     
     public boolean esDAW(){
         if(grupo.contains("DAW"))
             return true;
         else
             return false;
     }
}

