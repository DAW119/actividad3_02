/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package a4_3;

import java.util.Scanner;

/**
 *
 * @author DAW121
 */
public class Triatlon {
    private int dorsal;
    private String nombre;
    private int[] puntuaciones;
//Constructor
    public Triatlon(int dorsal, String nombre, int numPruebas) {
        this.dorsal = dorsal;
        this.nombre = nombre;
//Array
        this.puntuaciones = new int[numPruebas];
        
        Scanner sc = new Scanner(System.in);
        System.out.println("Inserte las puntuaciones: ");
        for (int i = 0; i < numPruebas; i++) {
            this.puntuaciones[i] = sc.nextInt();
        }
                
    }
 //Metodo pMedia   
    public float pMedia(){
        float suma=0;
        float media;
        for (int i = 0; i < this.puntuaciones.length; i++) {
            suma += this.puntuaciones[i];
        }
        media = suma/this.puntuaciones.length;
        return media;
    }
    
    public boolean esSeleccionado(){
        
        boolean seleccionado = false;
        
        for (int i = 0; i < this.puntuaciones.length; i++) {
            if(this.puntuaciones[i] > 10){
                seleccionado = true;
            }
        }
        return seleccionado;
    }
    
    public static void main(String[] args) {
        Triatlon t1 = new Triatlon(13, "Juan", 5);
        System.out.println("Media: "+t1.pMedia());
    }
}