/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package a4_4;

import java.util.Random;

/**
 *
 * @author DAW121
 */
public class Matriz {//Tutorial 12 Java - Matrices y Random.mp4 (bidimensional)
    private int matriz[][];
    private Random random;
 
    public Matriz(){
        matriz=new int[4][5];
        random=new Random();
    }
    public void llenarMatriz(){
        try{
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz.length; j++) {
                matriz[i][j]=random.nextInt(101);
            }
            
        }
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    public void imprimirMatriz(){
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz.length; j++) {
                System.out.println(matriz[i][j]+"\t");
            }
            System.out.println();
        }
    }
    public static void main(String[]args){
        Matriz m= new Matriz();
        m.llenarMatriz();
        m.imprimirMatriz();
    }   
}
